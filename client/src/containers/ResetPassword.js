import React, { Component } from "react";
import "./ResetPassword.css";
import { Link } from "react-router-dom";
import { FormGroup, Glyphicon, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import API from '../api';
import querySearch from "stringquery";

export default class ResetPassword extends Component {
    
  state = {
    code: "",
    email: "",
    password: "",
    emailSent: false,
    confirmed: false,
    confirmPassword: "",
    isConfirming: false,
    isSendingEmail: false
  };

  componentWillMount() {
    const query = querySearch(this.props.location.search);
    if (query.emailSent) { this.setState({emailSent: true}) }
  }

  validateCodeForm() {
    return this.state.email.length > 0;
  }

  validateResetForm() {
    return (
      this.state.password.length > 0 &&
      this.state.password === this.state.confirmPassword
    );
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleSendEmailClick = async event => {
    event.preventDefault();

    this.setState({ isSendingEmail: true });

    try {
      await API.post('auth/password', {
        email: this.state.email,
        redirect_url: window.location.origin + '/login/reset'
      });
      alert('Por favor revisa tu correo electrónico ('+this.state.email+') y haz clic en el enlace para recuperar tu contraseña.')
      this.props.history.push("/");
    } catch (e) {
      alert(e.message);
      this.setState({ isSendingEmail: false });
    }
  };

  handleConfirmClick = async event => {
    event.preventDefault();

    this.setState({ isConfirming: true });

    try {
      console.log(this.state);
      await API.put('auth/password', {
        password: this.state.password,
        password_confirmation: this.state.confirmPassword
      }, {headers: JSON.parse(sessionStorage.getItem('user'))});
      this.setState({ confirmed: true });
    } catch (e) {
      alert(e.message);
      this.setState({ isConfirming: false });
    }
  };

  renderRequestEmailForm() {
    return (
      <form onSubmit={this.handleSendEmailClick}>
        <FormGroup bsSize="large" controlId="email">
          <ControlLabel>Correo electrónico</ControlLabel>
          <FormControl
            autoFocus
            type="email"
            value={this.state.email}
            onChange={this.handleChange}
          />
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          loadingText="Enviando…"
          text="Enviar correo"
          isLoading={this.state.isSendingEmail}
          disabled={!this.validateCodeForm()}
        />
      </form>
    );
  }

  renderConfirmationForm() {
    return (
      <form onSubmit={this.handleConfirmClick}>
        <FormGroup bsSize="large" controlId="password">
          <ControlLabel>Nueva contraseña</ControlLabel>
          <FormControl
            type="password"
            value={this.state.password}
            onChange={this.handleChange}
          />
        </FormGroup>
        <FormGroup bsSize="large" controlId="confirmPassword">
          <ControlLabel>Confirmar contraseña</ControlLabel>
          <FormControl
            type="password"
            onChange={this.handleChange}
            value={this.state.confirmPassword}
          />
        </FormGroup>
        <LoaderButton
          block
          type="submit"
          bsSize="large"
          text="Cambiar"
          loadingText="Cambiando…"
          isLoading={this.state.isConfirming}
          disabled={!this.validateResetForm()}
        />
      </form>
    );
  }

  renderSuccessMessage() {
    return (
      <div className="success">
        <Glyphicon glyph="ok" />
        <p>Tu contraseña se ha restablecido exitosamente.</p>
        <p>
          <Link to="/login">
            Haz clic aquí para iniciar sesión.
          </Link>
        </p>
      </div>
    );
  }

  render() {
    return (
      <div className="ResetPassword">
        {!this.state.emailSent
          ? this.renderRequestEmailForm()
          : !this.state.confirmed
            ? this.renderConfirmationForm()
            : this.renderSuccessMessage()}
      </div>
    );
  }
}