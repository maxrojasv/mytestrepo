import React, { Component } from "react";
import "./Login.css";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import { Link } from "react-router-dom";
import API from '../api';
import Facebook from '../components/Facebook';
import Recaptcha from 'react-google-invisible-recaptcha';

export default class Login extends Component {

  state = {
    isLoading: false,
    email: "",
    password: ""
  };

  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      const response = await API.post('auth/sign_in', {
        email: this.state.email, 
        password: this.state.password
      });
      console.log(response);
      console.log(response.headers['access-token']);
      console.log(response.headers.client);
      console.log(response.headers.uid);
      sessionStorage.setItem('user', JSON.stringify({
        'access-token': response.headers['access-token'],
        client: response.headers.client,
        uid: response.headers.uid
      }));
      this.props.userHasAuthenticated(true);
      this.props.history.push("/");
    } catch (e) {
      alert(e.message);
      this.setState({ isLoading: false });
    }
  }

  

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Correo electrónico</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Contraseña</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Link to="/login/reset">¿Olvidó su contraseña?</Link>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Iniciar sesión"
            loadingText="Iniciando sesión…"
          />
          <Recaptcha
            ref={ ref => this.recaptcha = ref }
            sitekey="6Leh2pMUAAAAAFSHWyYVZOMi-bfpIJOXLKGuyA8O"
            onResolved={ () => console.log( 'Human detected.' ) } />
          <div className="row d-flex align-items-center justify-content-center kpx_row-sm-offset-3 kpx_loginOr">
            <div className="">
              <hr className="kpx_hrOr" ></hr>
              <span className="kpx_spanOr">o</span>
            </div>
          </div>
        </form>
          <Facebook />
      </div>
    );
  }
}