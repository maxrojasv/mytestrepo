import React, { Component, Fragment } from 'react'
import "./App.css";
import { Link, withRouter } from 'react-router-dom'
import { Nav, Navbar, NavItem } from "react-bootstrap";
import Routes from "./Routes";
import { LinkContainer } from "react-router-bootstrap";
import API from './api';
import querySearch from "stringquery";

class App extends Component {
	state = {
    isAuthenticated: false,
    isAuthenticating: true
  };

  async componentDidMount() {
	  try {
	  	const query = querySearch(this.props.location.search);
	  	if (query.reset_password) {
			  sessionStorage.setItem('user', JSON.stringify({
		      'access-token': query.token,
		      client: query.client,
		      uid: query.uid.replace('%40', '@')
		    }));
    		const response = await API.get('auth/validate_token', {headers: JSON.parse(sessionStorage.getItem('user'))});
	    	console.log(response);
    		this.props.history.push("/login/reset?emailSent=true");

	  	}
	  	else {
	    	const response = await API.get('auth/validate_token', {headers: JSON.parse(sessionStorage.getItem('user'))});
	    	console.log(response);
		    this.userHasAuthenticated(true);
	  	}
	  }
	  catch(e) {
    	this.props.history.push("/login");
	  }

	  this.setState({ isAuthenticating: false });
	}



  userHasAuthenticated = authenticated => {
	  this.setState({ isAuthenticated: authenticated });
	}

	handleLogout = async event => {
    const response = await API.delete('auth/sign_out', {headers: JSON.parse(sessionStorage.getItem('user'))});
    console.log(response);

    sessionStorage.removeItem('user');

    this.userHasAuthenticated(false);

    this.props.history.push("/login");
  }

  getQueryStringParams = query => {
    return query
      ? (/^[?#]/.test(query) ? query.slice(1) : query)
          .split('&')
          .reduce((params, param) => {
              let [key, value] = param.split('=');
              params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';
              return params;
          }, {})
      : {}
	}

  render() {


	  const childProps = {
	    isAuthenticated: this.state.isAuthenticated,
	    userHasAuthenticated: this.userHasAuthenticated
	  };

	  return (
	    !this.state.isAuthenticating &&
	    <div className="App container">
	      <Navbar fluid collapseOnSelect>
	        <Navbar.Header>
	          <Navbar.Brand>
	            <Link to="/">Zimple Math</Link>
	          </Navbar.Brand>
	          <Navbar.Toggle />
	        </Navbar.Header>
	        <Navbar.Collapse>
	          <Nav pullRight>
	            {this.state.isAuthenticated
	              ? <Fragment>
				            <LinkContainer to="/settings">
				              <NavItem>Settings</NavItem>
				            </LinkContainer>
				            <NavItem onClick={this.handleLogout}>Logout</NavItem>
				          </Fragment>
	              : <Fragment>
	                  <LinkContainer to="/signup">
	                    <NavItem>Signup</NavItem>
	                  </LinkContainer>
	                  <LinkContainer to="/login">
	                    <NavItem>Login</NavItem>
	                  </LinkContainer>
	                </Fragment>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Navbar>
	      <Routes childProps={childProps} />
	    </div>
	  );
	}
}

export default withRouter(App);