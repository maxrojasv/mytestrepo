import React, { Component } from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

export default class Facebook extends Component {
  state = {
    isLoggedIn: false,
    userID: "",
    accessToken:"",
    first_name: "",
    last_name: "",
    email: "",
    picture: ""
  };

  responseFacebook = (response) => {
    console.log(response);
    if (response.email) {
      this.setState({
        isLoggedIn: true,
        userID: response.userID,
        accessToken: response.accessToken,
        first_name: response.first_name,
        last_name: response.last_name,
        email: response.email,
        picture: response.picture.data.url
      });
    }
  }

  componentClicked = () => {
    console.log("facebook login button clicked");
  }

  render() {
    let fbContent;
    const fbStyle = {
      margin: '0 auto',
      maxWidth: '32rem'
    }

    if (this.state.isLoggedIn) {
      fbContent = (
        <div
          style={{
            width: "400px",
            margin: "auto",
            background: "#f4f4f4",
            padding: "20px"
          }}
        >
          <img src={this.state.picture} alt={this.state.name} />
          <h2>Welcome {this.state.name}</h2>
          Email: {this.state.email}
        </div>
      );
    } else {
      fbContent = (
        <FacebookLogin
          appId="248910375998483"
          fields="email,first_name,last_name,picture"
          scope="public_profile"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
          render={renderProps => (
            <button onClick={renderProps.onClick} className="login-fb-btn">Facebook</button>
          )}
        />
      );
    }

    return <div style={fbStyle} className="d-flex align-items-center justify-content-center">{fbContent}</div>;
  }
}