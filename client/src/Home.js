import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';
// import { FontAwesomeIcon } from 'react-fontawesome';
import 'bootstrap/dist/css/bootstrap.css';
import Facebook from '../components/Facebook';

const rowStyle = {height: '100%'};

class Home extends Component {
  render() {
    return (
        <div className="login-container">
          <MDBRow style={rowStyle} className="d-flex align-items-center justify-content-center">
            <MDBCol md="6">
              <form className="login-form">
                <p className="h5 text-center mb-4">Inicie sesión con</p>
                <Facebook />
                <div className="row d-flex align-items-center justify-content-center kpx_row-sm-offset-3 kpx_loginOr">
                  <div className="col-xs-12 col-sm-6">
                    <hr className="kpx_hrOr" ></hr>
                    <span className="kpx_spanOr">o</span>
                  </div>
                </div>
                <div className="grey-text">
                  <MDBInput
                    label="Escribe tu correo electrónico"
                    icon="envelope"
                    autofocus="true"
                    group
                    type="email"
                    validate
                    error="wrong"
                    success="right"
                  />
                  <MDBInput
                    label="Escribe tu contraseña"
                    icon="lock"
                    group
                    type="password"
                    validate
                  />
                </div>
                <div className="text-center">
                  <MDBBtn>Entrar</MDBBtn>
                </div>
                <div className="d-flex flex-column">
                  <div className="p-2">¿No tienes una cuenta? <a href="/registro">Regístrate</a></div>
                  <div className="p-2">¿Olvidaste tu contraseña? <a href="/recuperacion">Recupérala</a></div>
                </div>
              </form>
            </MDBCol>
          </MDBRow>
        </div>
    );
  }
}

export default Home;
