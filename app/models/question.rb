class Question
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :test
  has_many :alternatives, dependent: :destroy
  has_many :knowledge_questions, dependent: :destroy
  has_many :ability_questions, dependent: :destroy
  has_many :statements, dependent: :destroy
  has_many :resources, dependent: :destroy
  
  field :answer_state, type: Integer
  field :min_resp_time, type: Integer
  field :max_resp_time, type: Integer
  field :q_type, type: Integer
  field :g_factor, type: Integer
  field :answer_format, type: Integer
  field :max_score, type: Integer
  field :min_score, type: Integer

  validates :answer_state, :max_resp_time, :q_type, :answer_format, :max_score, :min_score, presence: true
end
