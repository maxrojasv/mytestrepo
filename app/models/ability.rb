class Ability
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  
  has_many :subabilities, dependent: :restrict_with_error
  
  field :name, type: String

  validates :name, presence: true

end
