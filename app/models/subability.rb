class Subability
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :ability
  has_many :ability_questions, dependent: :destroy

  field :name, type: String
  field :level, type: Integer
  field :description, type: String
  field :ability_type, type: Integer
end
