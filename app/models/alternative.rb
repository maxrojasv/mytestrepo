class Alternative
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :question
  has_many :user_alternatives, dependent: :restrict_with_error
  
  field :expected, type: Boolean, default: false
  field :right, type: Boolean, default: false

  validates :expected, :right, presence: true
end
