class Test
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :user
  has_many :questions, dependent: :restrict_with_error
  
  field :done, type: Boolean, default: false
  field :finished_at, type: Time
  field :test_type, type: Integer

  validates :done, :test_type, presence: true

end
