class Statement
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :question
  
  field :content, type: String

  validates :content, presence: true
end
