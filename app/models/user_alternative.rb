class UserAlternative
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  
  belongs_to :user
  belongs_to :alternative

  field :time, type: Integer

  validates :time, presence: true
end
