class KnowledgeQuestion
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :question
  belongs_to :knowledge_content
  
end
