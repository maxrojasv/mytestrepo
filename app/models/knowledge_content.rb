class KnowledgeContent
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :knowledge
  has_many :knowledge_questions, dependent: :restrict_with_error

  field :name, type: String
  field :level, type: Integer
  field :description, type: String
  field :success_factor, type: Float
end
