class Country
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  
  belongs_to :user
  has_many :country_knowledge, dependent: :restrict_with_error
  
  field :name, type: String
  
  validates :name, presence: true
end
