class Knowledge
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  has_many :knowledge_contents
  
  field :name, type: String
  
  validates :name, presence: true
end
