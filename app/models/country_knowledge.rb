class CountryKnowledge
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps

  belongs_to :country
  belongs_to :knowledge_content
  
end
