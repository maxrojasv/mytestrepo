class User
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  include Mongoid::Locker
  
  # Include default devise modules. Others available are: :confirmable, :lockable, :timeoutable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, 
         :trackable, :validatable, :omniauthable, omniauth_providers: %i[facebook]
  include DeviseTokenAuth::Concerns::User

  has_many :tests, dependent: :destroy
  has_many :user_alternatives, dependent: :destroy
  has_one :country
  

  field :first_name,             type: String
  field :last_name,              type: String
  field :age,                    type: Integer
  field :gender,                 type: Integer
  field :hometown,               type: String
  field :score,                  type: Integer
  field :exam_pred_score,        type: Integer
  field :g_factor,               type: Integer
  
  field :provider,               type: String
  field :uid,                    type: String, default: ''
  field :tokens,                 type: Hash,   default: {}
  # field :locker_locked_at,       type: Time
  # field :locker_locked_until,    type: Time

    ## Database authenticatable
  field :email,              :type => String, :default => ""
  field :encrypted_password, :type => String, :default => ""

  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Rememberable
  field :remember_created_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  ## Confirmable
  # field :confirmation_token,   :type => String
  # field :confirmed_at,         :type => Time
  # field :confirmation_sent_at, :type => Time
  # field :unconfirmed_email,    :type => String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, :type => Integer, :default => 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    :type => String # Only if unlock strategy is :email or :both
  # field :locked_at,       :type => Time

  ## Token authenticatable
  # field :authentication_token, :type => String

  field :allow_password_change, type: Boolean, default: false


  locker locked_at_field: :locker_locked_at,
         locked_until_field: :locker_locked_until


  index({ uid: 1, provider: 1}, { name: 'uid_provider_index', unique: true, background: true })
end

