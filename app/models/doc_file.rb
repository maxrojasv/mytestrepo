  # Storage
  # store_in collection: "citizens", database: "other", client: "secondary"
  # Note that the value supplied to the client option must be configured under clients in your mongoid.yml.Note that the value supplied to the client option must be configured under clients in your mongoid.yml.
  # store_in database: ->{ Thread.current[:database] }

  # Relationship names
  # embedded_in :alcoholic, class_name: "Lush", inverse_of: :whiskey
  # embeds_many :addresses, validate: false

  # Scopes
  # scope :active, ->{
  #   where(active: true) do
  #     def deutsch
  #       tap do |scope|
  #         scope.selector.store("origin" => "Deutschland")
  #       end
  #     end
  #   end
  # }

  # Valid field types:
  # Array
  # BigDecimal
  # Boolean
  # Date
  # DateTime
  # Float
  # Hash
  # Integer
  # BSON::ObjectId
  # BSON::Binary
  # Range
  # Regexp
  # String
  # Symbol
  # Time
  # TimeWithZone

  # Getters and setters
  # Get the value of the first_name field
  # person.first_name
  # person[:first_name]
  # person.read_attribute(:first_name)
  # Set the value for the first_name field
  # person.first_name = "Jean"
  # person[:first_name] = "Jean"
  # person.write_attribute(:first_name, "Jean")
  # person.attributes = { first_name: "Jean-Baptiste", middle_name: "Emmanuel" }
  # person.write_attributes(
  #   first_name: "Jean-Baptiste",
  #   middle_name: "Emmanuel"
  # )

  # Default values
  # field :blood_alcohol_level, type: Float, default: 0.40
  # field :last_drink, type: Time, default: ->{ new_record? ? 2.hours.ago : Time.now }
  # When defining a default value as a proc, Mongoid will apply the default after all other attributes are set. If you want this to happen before the other attributes, set pre_processed: true.
  # default values that are not defined as lambdas or procs are evaluated at class load time
  
  # Localization
  # field :description, localize: true
  # product.description_translations = { "en" => "Marvelous!", "de" => "Wunderbar!" }
  # Si se van a hacer queries con los campos localizados, agregar indexes a cada uno
  # index "description.es" => 1
  # index "description.en" => 1

  # Seeing changes
  # person.name = "Alan Garner"
  # # Check to see if the document has changed.
  # person.changed? # true
  # # Get an array of the names of the changed fields.
  # person.changed # [ :name ]
  # # Get a hash of the old and changed values for each field.
  # person.changes # { "name" => [ "Alan Parsons", "Alan Garner" ] }
  # # Check if a specific field has changed.
  # person.name_changed? # true
  # # Get the changes for a specific field.
  # person.name_change # [ "Alan Parsons", "Alan Garner" ]
  # person.name_was # "Alan Parsons"
  # person.reset_name!
  # person.name # "Alan Parsons"
  # person.save # Clears out current changes.
  # # View the previous changes.
  # person.previous_changes # { "name" => [ "Alan Parsons", "Alan Garner" ] }

  # Readonly attributes
  # attr_readonly :name, :origin
  # band.update_attribute(:name, "Tool") # Raises the error.
  # band.remove_attribute(:name) # Raises the error.

  # Inheritance
  # embeds_many :shapes
  # embedded_in :canvas
  # class Shape < Canvas
  # Builds a Shape object
  # firefox.shapes.build({ x: 0, y: 0 })
  # # Builds a Circle object
  # firefox.shapes.build({ x: 0, y: 0 }, Circle)

  # Timestamping
  # include Mongoid::Timestamps
  # If you want to turn off timestamping for specific calls, use the timeless method:
  # person.timeless.save

  # Dynamic fields
  # include Mongoid::Attributes::Dynamic
  # # Retrieve a dynamic field safely.
  # person[:gender]
  # person.read_attribute(:gender)
  # # Write a dynamic field safely.
  # person[:gender] = "Male"
  # person.write_attribute(:gender, "Male")

  # Capped collections (cache, logs)
  # client["name", :capped => true, :size => 1024].create

  # Collation
  # client["name", :collation => { :locale => 'fr'}].create

  # MapReduce
  # map = %Q{
  #   function() {
  #     emit(this.name, { likes: this.likes });
  #   }
  # }
  # reduce = %Q{
  #   function(key, values) {
  #     var result = { likes: 0 };
  #     values.forEach(function(value) {
  #       result.likes += value.likes;
  #     });
  #     return result;
  #   }
  # }
  # Band.where(:likes.gt => 100).map_reduce(map, reduce).out(inline: 1)
  # inline: 1: Don’t store the output in a collection.
  # replace: "name": Store in a collection with the provided name, and overwrite any documents that exist in it.
  # merge: "name": Store in a collection with the provided name, and merge the results with the existing documents.
  # reduce: "name": Store in a collection with the provided name, and reduce all existing results in that collection.

  # Polymorphism
  # embedded_in :photographic, polymorphic: true
  # embeds_many :photos, as: :photographic

  # Callbacks
  # embeds_many :albums, cascade_callbacks: true
  # band.save # Fires all save callbacks on the band, albums, and label.

  # Autosave
  # has_many :albums, autosave: true
  # band.albums.build(name: "101")
  # band.save # Will save the album as well.

  # Recursing embedding
  # recursively_embeds_many
  # root = Tag.new(name: "programming")
  # child_one = root.child_tags.build
  # child_two = root.child_tags.build

  # Existence
  # embeds_one :label
  # band.label?
  # band.has_label?

  # Document callbacks
  # after_initialize
  # after_build
  # before_validation
  # after_validation
  # before_create
  # around_create
  # after_create
  # after_find
  # before_update
  # around_update
  # after_update
  # before_upsert
  # around_upsert
  # after_upsert
  # before_save
  # around_save
  # after_save
  # before_destroy
  # around_destroy
  # after_destroy

  # Collection callbacks
  # after_add
  # after_remove
  # before_add
  # before_remove
  # has_many :posts, after_add: :send_email_to_subscribers

  # Indexes
  # field :ssn
  # index({ ssn: 1 }, { unique: true, name: "ssn_index" })
  # embeds_many :addresses
  # index "addresses.street" => 1
  # index({ first_name: 1, last_name: 1 }, { unique: true })
  # index({ ssn: -1 }, { sparse: true })
  # index({ ssn: 1 }, { unique: true, background: true })
  # field :location, type: Array
  # index({ location: "2d" }, { min: -200, max: 200 })
  # index({ ssn: 1 }, { database: "users", unique: true, background: true })
  # belongs_to :post, index: true
  # has_and_belongs_to_many :preferences, index: true
  # Model.create_indexes
  # Model.remove_indexes

  # Queries
  # where(:founded.gte => "1980-1-1").
  # in(name: [ "Tool", "Deftones" ]).
  # union.
  # in(name: [ "Melvins" ])

  # Rake tasks
  # db:create_indexes: Reads all index definitions from the models and attempts to create them in the database.
  # db:remove_indexes: Reads all secondary index definitions from the models.
  # db:drop:           Drops all collections in the database with the exception of the system collections.
  # db:purge:          Deletes all data, including indexes, from the database. Since 3.1.0
  # db:seed:           Seeds the database from db/seeds.rb
  # db:setup:          Creates indexes and seeds the database.
  # Useless tasks
  # db:create:         Exists only for dependency purposes, does not actually do anything.
  # db:migrate:        Exists only for dependency purposes, does not actually do anything.
  # db:schema:load:    Exists only for framework dependency purposes, does not actually do anything.
  # db:test:prepare:   Exists only for framework dependency purposes, does not actually do anything.

  # Dump and restore
  # mongodump --host your.production.mongo.server.ip --port 37017 --username user --password --out /Users/AlohaCC/Desktop/production-mongodump-2015-10-04
  # mongorestore --host localhost --port 3017 --username user --password  /Users/AlohaCC/Desktop/production-mongodump-2015-10-04



  # Custom types
  # field :location, type: Point # Example of custom type. See end of this file
  # point = Point.new(12, 24)
  # venue = Venue.new(location: point) # This uses the mongoize instance method.
  # venue = Venue.new(location: [ 12, 24 ]) # This uses the mongoize class method.
  # class Point

  #   attr_reader :x, :y

  #   def initialize(x, y)
  #     @x, @y = x, y
  #   end

  #   # Converts an object of this instance into a database friendly value.
  #   def mongoize
  #     [ x, y ]
  #   end

  #   class << self

  #     # Get the object as it was stored in the database, and instantiate
  #     # this custom class from it.
  #     def demongoize(object)
  #       Point.new(object[0], object[1])
  #     end

  #     # Takes any possible object and converts it to how it would be
  #     # stored in the database.
  #     def mongoize(object)
  #       case object
  #       when Point then object.mongoize
  #       when Hash then Point.new(object[:x], object[:y]).mongoize
  #       else object
  #       end
  #     end

  #     # Converts the object that was supplied to a criteria and converts it
  #     # into a database friendly form.
  #     def evolve(object)
  #       case object
  #       when Point then object.mongoize
  #       else object
  #       end
  #     end
  #   end
  # end