class Resource
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  include Mongoid::Timestamps
  include Mongoid::Paperclip

  belongs_to :question

  has_mongoid_attached_file :attachment
end
