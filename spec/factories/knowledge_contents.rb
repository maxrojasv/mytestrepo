FactoryGirl.define do
  factory :knowledge_content do
    name "MyString"
    level 1
    description "MyString"
    success_factor 1.5
  end
end
