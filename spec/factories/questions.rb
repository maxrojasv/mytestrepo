FactoryGirl.define do
  factory :question do
    answer_state 1
    min_resp_time 1
    max_resp_time 1
    q_type 1
    g_factor 1
    answer_format 1
    max_score 1
    min_score 1
  end
end
