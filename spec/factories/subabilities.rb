FactoryGirl.define do
  factory :subability do
    name "MyString"
    level 1
    description "MyString"
    ability_type 1
  end
end
