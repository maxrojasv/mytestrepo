Rails.application.config.middleware.use OmniAuth::Builder do
  provider :facebook, ENV['FB_APP_ID'], ENV['FB_APP_SECRET'],
    scope: 'email,public_profile,user_birthday,user_gender,user_hometown', display: 'touch'
  # provider :google_oauth2, ENV['G_APP_KEY'], ENV['G_APP_SECRET']
end

# https://developers.facebook.com/docs/facebook-login/permissions