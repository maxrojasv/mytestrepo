# Zimple Math

* Ruby 2.6.1

* Rails 5.2.2

###### Dependencias del sistema

- [MongoDb](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)

- Create React App: `npx create-react-app client`

###### Cómo ejecutar la aplicación

1. Ejecutar `yarn --cwd client install`
2. Ejecutar `yarn --cwd client start`
3. Ejecutar en otra pestaña del terminal `rails s -p 3001`
4. Ejecutar en otra pestaña del terminal `mongod`


###### Cómo agregar librerías React
`yarn --cwd client add <nombre>`


Be happy!
Be happy2!
Be happy3!